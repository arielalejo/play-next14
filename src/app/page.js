import Image from 'next/image'
import styles from './page.module.css'
import dog from '../../assets/dog.jpeg'

export default function Home() {
  return (
    <main className={styles.main}>
      <img src="../../assets/dog.jpeg"/>
    </main>
  )
}
